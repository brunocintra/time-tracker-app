import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class CheckButtonDay extends StatelessWidget {
  const CheckButtonDay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          decoration: BoxDecoration(
              color: HexColor('E9E9FF'),
              borderRadius: BorderRadius.circular(12)),
          // color: HexColor('E9E9FF'),
          width: 300,
          child: Row(
            children: [
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.fromLTRB(6, 0, 3, 0),
                child: TextButton(
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(HexColor('FFFFFF'))),
                  child: Text(
                    'Day',
                    style: TextStyle(color: Color.fromRGBO(7, 4, 23, 1)),
                  ),
                  onPressed: () => {},
                ),
              )),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.fromLTRB(3, 0, 6, 0),
                child: TextButton(
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(HexColor('E9E9FF'))),
                  child: Text(
                    'Week',
                    style: TextStyle(
                      color: Color.fromRGBO(7, 4, 23, 0.4),
                    ),
                  ),
                  onPressed: () => {},
                ),
              ))
            ],
          )),
    );
  }
}
