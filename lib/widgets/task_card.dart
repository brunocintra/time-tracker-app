import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:time_tracker_app/model/task.dart';
import 'package:time_tracker_app/page/timer_page.dart';

class TaskCard extends StatelessWidget {
  const TaskCard({Key? key, required this.task}) : super(key: key);
  final Task task;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: Container(
        width: double.infinity,
        height: 100,
        decoration: new BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(15)),
        child: Padding(
            padding: EdgeInsets.fromLTRB(10, 16, 2, 8),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 8, 16, 8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset("assets/images/" + task.imagePath + ".png")
                    ],
                  ),
                ),
                Container(
                  width: 240,
                  // color: Colors.red,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          // color: Colors.black45,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              ConstrainedBox(
                                  constraints: BoxConstraints(maxWidth: 180),
                                  child: Text(
                                    task.name,
                                    style: taskNameStyle(),
                                  )),
                              Text(
                                task.usedTime,
                                style: timeStyle(),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Row(
                                  children: [
                                    taskTag(task.tags, Colors.black87,
                                        Colors.black12)
                                  ],
                                ),
                              ],
                            ),
                            Column(children: [
                              Row(children: [
                                TextButton(
                                  child: Icon(Icons.play_arrow,
                                      color: Colors.black),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => TimerPage(
                                                  task: task,
                                                )));
                                  },
                                ),
                              ])
                            ])
                          ],
                        ),
                      ]),
                ),
              ],
            )),
      ),
    );
  }

  Padding taskTag(
      String textTask, Color foregroundColor, Color backgroundColor) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Container(
        padding: EdgeInsets.all(6),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10), color: backgroundColor),
        child: Text(
          textTask,
          style: tagStyle(foregroundColor),
        ),
      ),
    );
  }

  TextStyle taskNameStyle() {
    return TextStyle(
        fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black);
  }

  TextStyle timeStyle() {
    return TextStyle(
        fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black45);
  }

  TextStyle tagStyle(Color? myColor) {
    return TextStyle(
        fontSize: 16, height: 1, fontWeight: FontWeight.w400, color: myColor);
  }
}
