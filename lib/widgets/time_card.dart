import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TimeCard extends StatelessWidget {
  const TimeCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 350,
      height: 100,
      decoration: new BoxDecoration(
          color: Colors.black, borderRadius: BorderRadius.circular(15)),
      child: Padding(
          padding: EdgeInsets.fromLTRB(12, 10, 0, 10),
          child: Column(children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "00:32:10",
                  style: timeStyle(),
                ),
                TextButton(
                    onPressed: () {},
                    child: Image.asset("assets/images/right arrow-2.png"))
              ],
            ),
            Row(
              children: [
                Image.asset("assets/images/Ellipse.png"),
                Padding(
                  padding: const EdgeInsets.only(left: 8),
                  child: Text("Project name",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      )),
                ),
              ],
            )
          ])),
    );
  }

  TextStyle timeStyle() {
    return TextStyle(
        fontSize: 32, fontWeight: FontWeight.bold, color: Colors.white);
  }
}
