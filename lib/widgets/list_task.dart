import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ListTask extends StatelessWidget {
  const ListTask({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 24),
      child: Container(
        width: 350,
        height: 400,
        decoration: new BoxDecoration(color: Colors.grey.shade100),
        child: Column(children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text(
              "Today",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
            ),
            Text(
              "See all",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
            )
          ]),
          // TaskCard(),
          // TaskCard(),
          // TaskCard()
        ]),
      ),
    );
  }
}
