import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:time_tracker_app/model/task.dart';
import 'package:time_tracker_app/services/tasks_service.dart';
import 'package:time_tracker_app/widgets/task_card.dart';

class ListviewTask extends StatefulWidget {
  ListviewTask({Key? key}) : super(key: key);

  @override
  _ListviewTaskState createState() => _ListviewTaskState();
}

class _ListviewTaskState extends State<ListviewTask> {
  var taskService = TaskService();
  late Future<List<Task>> allTasks;

  @override
  void initState() {
    super.initState();
    allTasks = taskService.fetchAllTasks();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<List<Task>>(
        future: allTasks,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: FittedBox(
                      fit: BoxFit.fill, // otherwise the logo will be tiny
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ]);
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          } else {
            return ListView.builder(
                itemBuilder: (context, index) {
                  final item = snapshot.data![index];
                  return Dismissible(
                      // Each Dismissible must contain a Key. Keys allow Flutter to
                      // uniquely identify widgets.
                      key: Key(item.id.toString()),
                      // Provide a function that tells the app
                      // what to do after an item has been swiped away.
                      onDismissed: (direction) {
                        // Remove the item from the data source.
                        setState(() {
                          taskService.removeTask(snapshot.data![index]);
                          snapshot.data!.removeAt(index);
                        });

                        // // Then show a snackbar.
                        // ScaffoldMessenger.of(context).showSnackBar(
                        //     SnackBar(content: Text('$item dismissed')));
                      },
                      child: TaskCard(task: snapshot.data![index]));
                  // return TaskCard(
                  //   task: snapshot.data![index],
                  // );
                },
                itemCount: snapshot.data!.length);
          }
        },
      ),
    );
  }
}
