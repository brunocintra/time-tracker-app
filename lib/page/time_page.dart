import 'package:flutter/material.dart';
import 'package:textfield_tags/textfield_tags.dart';
import 'package:time_tracker_app/model/task.dart';
import 'package:time_tracker_app/services/tasks_service.dart';

class TimePage extends StatefulWidget {
  TimePage({Key? key}) : super(key: key);

  @override
  _TimePageState createState() => _TimePageState();
}

class _TimePageState extends State<TimePage> {
  var taskService = TaskService();
  var _controllerName = TextEditingController();
  var tags = List<String>.empty(growable: true);

  Task createTask() {
    return Task(
        name: _controllerName.text,
        imagePath: "html",
        usedTime: "00:00:00",
        tags: tags.join(' '));
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.grey.shade100,
        title: Container(
          child: Image.asset(
            "assets/images/timepad v2.png",
            width: 150,
          ),
        ),
      ),
      body: Center(
          child: Column(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(18.0),
            alignment: Alignment.center,
            height: 185,
            width: 400,
            decoration: new BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(15)),
            child: Container(
                child: Column(
              children: [
                TextField(
                  controller: _controllerName,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Name',
                  ),
                ),
                TextFieldTags(
                  tagsStyler: TagsStyler(
                      tagTextStyle: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                      tagDecoration: BoxDecoration(
                        color: Colors.purple,
                        borderRadius: BorderRadius.circular(0.0),
                      ),
                      tagCancelIcon:
                          Icon(Icons.cancel, size: 18.0, color: Colors.white),
                      tagPadding: const EdgeInsets.all(6.0)),
                  textFieldStyler: TextFieldStyler(hintText: "Tags"),
                  onTag: (tag) {
                    tags.add(tag);
                  },
                  onDelete: (tag) {
                    tags.remove(tag);
                  },
                )
              ],
            )),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 100, 0, 0),
            child: Container(
                padding: const EdgeInsets.all(0.0),
                alignment: Alignment.center,
                //height: 185,
                width: 400,
                decoration: new BoxDecoration(
                    color: Colors.purple,
                    borderRadius: BorderRadius.circular(0)),
                child: Column(
                  children: [
                    TextButton(
                      style: TextButton.styleFrom(
                        padding: const EdgeInsets.all(16.0),
                        primary: Colors.white,
                        textStyle: const TextStyle(fontSize: 20),
                      ),
                      onPressed: () {
                        taskService.createTask(createTask());
                        setState(() {});
                      },
                      child: const Text('Salvar'),
                    ),
                  ],
                )), //Container
          ), //Padding
        ],
      )),
    );
  }
}
