import 'package:flutter/material.dart';
import 'package:time_tracker_app/widgets/listview_task.dart';
import 'package:time_tracker_app/widgets/time_card.dart';

class TaskPage extends StatelessWidget {
  const TaskPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.grey.shade100,
        title: Container(
          child: Image.asset(
            "assets/images/timepad v2.png",
            width: 150,
          ),
        ),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        // color: Colors.black12,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Task",
                  textAlign: TextAlign.start,
                  style: pageTitleStyle(),
                ),
                TextButton(
                    onPressed: () {},
                    child: Text(
                      "...",
                      style: pageTitleStyle(),
                    ))
              ],
            ),
            TimeCard(),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Today",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                    Text(
                      "See all",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                    )
                  ]),
            ),
            Expanded(
              child: ListviewTask(),
            ),
          ],
        ),
      ),
    );
  }

  TextStyle pageTitleStyle() {
    return TextStyle(
        fontSize: 20, fontWeight: FontWeight.w600, color: Colors.black);
  }

  TextStyle appTitleStyle() {
    return TextStyle(
        fontSize: 20, fontWeight: FontWeight.w600, color: Colors.black);
  }
}
