import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gradient_progress_indicator/widget/gradient_progress_indicator_widget.dart';
import 'package:time_tracker_app/model/task.dart';
import 'package:time_tracker_app/services/tasks_service.dart';

class TimerPage extends StatefulWidget {
  final Task task;

  const TimerPage({Key? key, required this.task}) : super(key: key);

  @override
  _TimerPageState createState() => _TimerPageState(task);
}

class _TimerPageState extends State<TimerPage> {
  var taskService = TaskService();
  bool isTimerPlaying = false;
  late Timer _timer;
  int seconds = 0;
  int minutes = 0;
  int hours = 0;
  late Task task;

  _TimerPageState(this.task) {
    var splitedTime = task.usedTime.split(':');
    hours = int.parse(splitedTime[0]);
    minutes = int.parse(splitedTime[1]);
    seconds = int.parse(splitedTime[2]);
  }

  List<Color> gradientColors = const [
    Color(0xFFFFFFFF),
    Color(0xFFebebeb),
  ];

  String getFormattedTime(int hours, int minutes, int seconds) {
    return '${formatHours(hours)}:${formatMinuts(minutes)}:${formatSeconds(seconds)}';
  }

  String formatHours(int hours) {
    int hoursSize = hours.toString().length;
    return hoursSize < 2 ? "0$hours" : "$hours";
  }

  String formatMinuts(int minutes) {
    int minutesSize = minutes.toString().length;
    return minutesSize < 2 ? "0$minutes" : "$minutes";
  }

  String formatSeconds(int seconds) {
    int secondsSize = seconds.toString().length;
    return secondsSize < 2 ? "0$seconds" : "$seconds";
  }

  void changeTimerState() {
    setState(() {
      if (isTimerPlaying) {
        _timer.cancel();
        gradientColors = const [
          Color(0xFFFFFFFF),
          Color(0xFFebebeb),
        ];
      } else {
        startTimer();
        gradientColors = const [
          Color(0xFFFFFFFF),
          Color(0xFF7012CE),
        ];
      }
      isTimerPlaying = !isTimerPlaying;
    });
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          _timer = timer;
          if (seconds < 0) {
            timer.cancel();
          } else {
            seconds = seconds + 1;
            if (seconds > 59) {
              minutes += 1;
              seconds = 0;
              if (minutes > 59) {
                hours += 1;
                minutes = 0;
              }
            }
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text(
              "Ui design",
            ),
            // Image.asset('assets/images/timer.png'),
            GradientProgressIndicator(
              duration: 6,
              radius: 120,
              strokeWidth: 24,
              gradientStops: const [
                0.2,
                0.7,
              ],
              gradientColors: gradientColors,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    getFormattedTime(hours, minutes, seconds),
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 32,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        changeTimerState();
                      },
                      child: Icon(
                          isTimerPlaying ? Icons.pause : Icons.play_arrow,
                          color: Colors.white),
                      style: ElevatedButton.styleFrom(
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(20),
                        primary: Color(0xFFE9E9FF), // <-- Button color
                        onPrimary: Colors.grey, // <-- Splash color
                      ),
                    ),
                    SizedBox(height: 16),
                    Text(isTimerPlaying ? "pause" : "play"),
                  ],
                ),
                Column(
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        task.usedTime =
                            getFormattedTime(hours, minutes, seconds);
                        taskService.updateTask(task);
                        Navigator.pop(context);
                      },
                      child: Icon(Icons.stop, color: Colors.white),
                      style: ElevatedButton.styleFrom(
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(20),
                        primary: Color(0xFFE9E9FF), // <-- Button color
                        onPrimary: Colors.grey, // <-- Splash color
                      ),
                    ),
                    SizedBox(height: 16),
                    Text("Quit"),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
