import 'package:flutter/material.dart';
import 'package:time_tracker_app/page/report_page.dart';
import 'package:time_tracker_app/page/task_page.dart';
import 'package:time_tracker_app/page/time_page.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentPage = 0;

  final List<Widget> _pages = [
    TaskPage(),
    TimePage(),
    ReportPage(),
  ];

  @override
  Widget build(BuildContext context) {
    PageController pageController = PageController(initialPage: _currentPage);

    return Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.timer,
                  size: 28.0,
                ),
                label: ''),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.add_circle_rounded,
                  size: 52.0,
                  color: Colors.black,
                ),
                label: ''),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.pie_chart_rounded,
                  size: 28.0,
                  color: Colors.black,
                ),
                label: '')
          ],
          currentIndex: _currentPage,
          onTap: (index) => {
            setState(() {
              pageController.jumpToPage(index);
              _currentPage = index;
            })
          },
        ),
        body: PageView(
          scrollDirection: Axis.horizontal,
          controller: pageController,
          children: _pages,
          onPageChanged: (page) {
            setState(() {
              _currentPage = page;
            });
          },
        ));
  }
}
