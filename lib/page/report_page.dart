import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:time_tracker_app/widgets/check_button_day.dart';
import 'package:time_tracker_app/widgets/task_completed.dart';
import 'package:time_tracker_app/widgets/time_duration.dart';

class ReportPage extends StatelessWidget {
  const ReportPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.grey.shade100,
          title: Container(
            child: Image.asset(
              "assets/images/timepad v2.png",
              width: 150,
            ),
          ),
        ),
        body: Container(
            child: Column(
          children: <Widget>[
            Container(
                height: 160,
                child: Column(
                  children: <Widget>[
                    Expanded(
                        child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: TaskCompleted(),
                            ),
                            flex: 1),
                        Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: TimeDuration(),
                            ),
                            flex: 1),
                      ],
                    )),
                  ],
                )),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CheckButtonDay(),
            )
          ],
        )));
  }
}
