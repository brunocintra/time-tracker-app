import 'package:flutter/material.dart';
import 'package:time_tracker_app/page/home_page.dart';
import 'package:time_tracker_app/page/report_page.dart';
import 'package:time_tracker_app/page/task_page.dart';
import 'package:time_tracker_app/page/time_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Time Tracker',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: Colors.grey.shade100,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => HomePage(),
        '/report': (context) => ReportPage(),
        '/task': (context) => TaskPage(),
        '/time': (context) => TimePage(),
      },
    );
  }
}
