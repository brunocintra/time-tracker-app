import 'dart:convert';
import 'dart:developer';

import 'package:time_tracker_app/model/task.dart';
import 'package:http/http.dart' as http;

class TaskService {
  Future<Task> fetchTask(int id) async {
    final response = await http
        .get(Uri.parse('https://app-timepad.herokuapp.com/timepad/task$id'));

    if (response.statusCode == 200) {
      return Task.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Deu erro!!');
    }
  }

  Future<List<Task>> fetchAllTasks() async {
    final response = await http
        .get(Uri.parse('https://app-timepad.herokuapp.com/timepad/task'));

    if (response.statusCode == 200) {
      Iterable list = jsonDecode(response.body);
      return list.map((task) => Task.fromJson(task)).toList();
    } else {
      throw Exception('Deu erro!!');
    }
  }

  Future<Task> createTask(Task task) async {
    final response = await http.post(
      Uri.parse('https://app-timepad.herokuapp.com/timepad/task'),
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      body: task.toJson(),
    );

    if (response.statusCode == 201 || response.statusCode == 200) {
      log("Inserido com sucesso");
      return Task.fromJson(jsonDecode(response.body));
    } else {
      throw Exception(
          'Deu erro no post: statusCode: ${response.statusCode} -> Error: ${response.body}');
    }
  }

  Future<Task> updateTask(Task task) async {
    var value = task.toJson();
    final response = await http.put(
      Uri.parse('https://app-timepad.herokuapp.com/timepad/task'),
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      body: value,
    );

    if (response.statusCode == 201 || response.statusCode == 200) {
      log("update com sucesso");
      return Task.fromJson(jsonDecode(response.body));
    } else {
      throw Exception(
          'Deu erro no put: statusCode: ${response.statusCode} -> Error: ${response.body}');
    }
  }

  Future<bool> removeTask(Task task) async {
    var value = task.toJson();
    final response = await http.delete(
      Uri.parse('https://app-timepad.herokuapp.com/timepad/task'),
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      body: value,
    );

    if (response.statusCode == 201 || response.statusCode == 200) {
      log("delete com sucesso");
      return true;
    } else {
      throw Exception(
          'Deu erro no put: statusCode: ${response.statusCode} -> Error: ${response.body}');
    }
  }
}
