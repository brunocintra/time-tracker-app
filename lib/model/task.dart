import 'dart:convert';

class Task {
  final int id;
  final String name;
  final String imagePath;
  late String usedTime;
  final String tags; // work cesar leonardo luiz
  Task({
    this.id = 0,
    required this.name,
    required this.imagePath,
    required this.usedTime,
    required this.tags,
  });

  Task copyWith({
    String? name,
    String? imagePath,
    String? usedTime,
    String? tags,
  }) {
    return Task(
      name: name ?? this.name,
      imagePath: imagePath ?? this.imagePath,
      usedTime: usedTime ?? this.usedTime,
      tags: tags ?? this.tags,
    );
  }

  String toJson() => json.encode(toMap());

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'imagePath': imagePath,
      'usedTime': usedTime,
      'tags': tags,
    };
  }

  factory Task.fromMap(Map<String, dynamic> map) {
    return Task(
      id: map['id'],
      name: map['name'],
      imagePath: map['imagePath'],
      usedTime: map['usedTime'],
      tags: map['tags'],
    );
  }

  factory Task.fromJson(Map<String, dynamic> json) {
    return Task(
        id: json['id'],
        name: json['name'],
        imagePath: json['imagePath'],
        usedTime: json['usedTime'],
        tags: json['tags']);
  }

  @override
  String toString() {
    return 'Task(id: $id, name: $name, imagePath: $imagePath, usedTime: $usedTime, tags: $tags)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Task &&
        other.name == name &&
        other.imagePath == imagePath &&
        other.usedTime == usedTime &&
        other.tags == tags;
  }

  @override
  int get hashCode {
    return name.hashCode ^
        imagePath.hashCode ^
        usedTime.hashCode ^
        tags.hashCode;
  }
}
